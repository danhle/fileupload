/*
 Danh Le z3463593
 INFS3605
 */
package fileupload;

import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

/**
 *
 * @author Danh Le | z3463593
 */
public class FileUploadController implements Initializable {

    //FXML Resources
    @FXML
    private TextField courseCodeTF;
    @FXML
    private Text uploadTF;
    @FXML
    private Button choosePDFButton;
    @FXML
    private Button addCourseButton;
    @FXML
    private Text statusMessage;
    @FXML
    private Text courseCodeDisplayTF;
    @FXML
    private Button downloadCourseOutline;

    //File chooser and object
    FileChooser fileChooser = new FileChooser();
    File selectedFile;
    private Desktop desktop = Desktop.getDesktop();

    //Database connection parameters
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "128.199.118.92:3306";

    //Database credentials
    static final String USER = "fileUpload";
    static final String PASS = "Ga8CRmYO08VKKG3ANNRNnTZ2CDd4bqx3EPCj2Wyj40pNEqD8Zi";
    Connection myConn = null;
    PreparedStatement myStmt = null;
    FileInputStream input = null;

    //Initialising values
    String courseCode;
    Blob courseOutline;

    @FXML
    private void choosePDF(ActionEvent event) {
        selectedFile = fileChooser.showOpenDialog(null);
        uploadTF.setText(selectedFile.getName());
    }

    private void openConnection() throws Exception {
        //Set up mySQL connection
        myConn = DriverManager.getConnection("jdbc:mysql://128.199.118.92:3306/FileUpload", USER, PASS);
        System.out.println("Connected to DB ...");
    }

    private Connection openConnectionReturn() throws Exception {
        //Set up mySQL connection
        myConn = DriverManager.getConnection("jdbc:mysql://128.199.118.92:3306/FileUpload", USER, PASS);
        System.out.println("Connected to DB ...");
        return myConn;
    }

    private void closeConnection() throws Exception {
        myConn.close();
        System.out.println("Connection closed");
    }

    @FXML
    private void addCourse(ActionEvent event) throws Exception {

        //Select a file using FileChooser
        //Read the file as a Blob data type, ready for addCourse
        courseCode = courseCodeTF.getText();
        courseCode = courseCodeTF.getText();
        System.out.print("Course Code: " + courseCode + "\n");

        openConnection();

        // 2. Prepare statement
        //String upload = "INSERT INTO main_table (course_code)" + " VALUES (?)";
        System.out.print("Insert query created ...\n");
        String upload = "INSERT INTO main_table (course_code, course_outline)" + " VALUES (?,?)";
        myStmt = myConn.prepareStatement(upload);

        //Insert courseCode as first paramater
        myStmt.setString(1, courseCode);

        //Upload file as second paramater
        InputStream pdfStream = (InputStream) new FileInputStream(selectedFile);
        myStmt.setBinaryStream(2, pdfStream);
        System.out.print("Reading PDF for upload ...\n");

        //Wait until PDF is read before closing input reader
        if (input != null) {
            input.close();
        }

        //Insert courseCode and PDF into mySQL
        System.out.print("Executing query ... \n");
        myStmt.executeUpdate();

        System.out.print("Insert successful!\n");
        statusMessage.setText("Status: " + courseCode + " Upload Complete");
        courseCodeDisplayTF.setText("Course Code: " + courseCode);

        //Close mySQL connection
        closeConnection();
    }

    //Retrieve PDF of most recent course added
    @FXML
    public void retrievePDF() throws Exception {

        //Initialise file values
        InputStream input = null;
        FileOutputStream output = null;

        //1. Connect to the Database
        openConnection();

        // 2. Execute statement
        String selectSQL = "SELECT course_outline FROM main_table WHERE course_code = ?";
        myStmt = myConn.prepareStatement(selectSQL);
        System.out.println("Finding your file...");

        myStmt.setString(1, courseCode);
        ResultSet rs = myStmt.executeQuery();

        // 3. Set up a handle to the file
        File theFile = new File("" + courseCode + " Outline.pdf");
        output = new FileOutputStream(theFile);

        if (rs.next()) {

            //Start downloading the PDF
            input = rs.getBinaryStream("course_outline");
            System.out.println("Downloading course outline from database...");

            //Keep downloading byte stream of PDF until 0
            byte[] buffer = new byte[1024];
            while (input.read(buffer) > 0) {
                output.write(buffer);
            }

            //Close the stream
            output.close();
            statusMessage.setText("\nSaved to file: " + theFile.getAbsolutePath() + "\n Download Successful");
        }

        //Open PDF using default PDF program    
        Desktop.getDesktop().open(theFile);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }
    
    public void test(){
        
    }
}
